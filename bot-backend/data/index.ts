
import * as config from 'config';
import * as pg from 'pg';

import { Sequelize } from 'sequelize';

// import { Preset } from './models';

// import * as models from './models';


const conString = config.get("db_connect");

// init sequelize and db

 const sequelizeInstance = new Sequelize(conString)

export const sequelize = sequelizeInstance;

const db: any = {}

// let models = [Preset];

// models.forEach(function(model) {
//     module.exports[model] = sequelize.import(__dirname + '/' + model);
//   });

export const InitDb = async () => {
    await sequelize.sync();

    console.log( 'we are here!!')
    // await models.sequelize.sync();
}


