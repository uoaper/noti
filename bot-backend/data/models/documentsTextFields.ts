import { DataTypes } from 'sequelize';

import { sequelize } from '../index'

const db = sequelize;

export interface IDocumentTextField {
  id?: number;
  documentId: number;
  value: Text,
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
}
export const DocumentTextField: any = db.define('documentTextField', {
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  documentId: {
    type: DataTypes.INTEGER
  },
  value: {
    type: DataTypes.TEXT
  }
}, {
  // Other model options go here
  paranoid: true,
  timestamps: true
});

