import { DataTypes } from 'sequelize';

import  { sequelize }  from '../index'

const db = sequelize;

export const Migration: any = db.define('_migration', {
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
},
  migration: {
    type: DataTypes.JSON 
    // allowNull defaults to true
  }
}, {
  // Other model options go here
  paranoid: true,
  timestamps: true
});

