import { DataTypes } from 'sequelize';

import  { sequelize }  from '../index'

const db = sequelize;

export interface IMain {
  id?: number;
  text: string;
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
}
export const Main: any = db.define('main', {
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  adversity: {
    type: DataTypes.CHAR
  },
  belief: {
    type: DataTypes.CHAR
  },
  conseq: {
    type: DataTypes.CHAR
  },
  disput: {
    type: DataTypes.CHAR
  },
  energy: {
    type: DataTypes.CHAR
  },
  beforescore: {
    type: DataTypes.INTEGER,
  },
  afterscore: {
  type: DataTypes.INTEGER,
  },
  datetime: {
  type: DataTypes.TIME,
  },
  user_id: {
  type: DataTypes.CHAR
  },
  token: {
  type: DataTypes.CHAR
  },
  user_id_by_token_holder: {
  type: DataTypes.CHAR
  },
  token_holder: {
  type: DataTypes.CHAR
  },
  google_id: {
  type: DataTypes.CHAR
  },
  facebook_id: {
  type: DataTypes.CHAR
  },
  google_token: {
  type: DataTypes.CHAR
  },
  facebook_token: {
  type: DataTypes.CHAR
  },
  uuid: {
  type: DataTypes.INTEGER,
  }
}, {
  // Other model options go here
  freezeTableName: true,
  paranoid: true,
  timestamps: true
});

