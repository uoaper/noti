import { DataTypes } from 'sequelize';
import { Json } from 'sequelize/types/lib/utils';

import  { sequelize }  from '../index'

const db = sequelize;


export interface IPreset {
  id?: number;
  name: string;
  json: Json,
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
}
export const Preset: any = db.define('preset', {
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
},
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  json: {
    type: DataTypes.JSON 
    // allowNull defaults to true
  }
}, {
  // Other model options go here
  paranoid: true,
  timestamps: true
});

