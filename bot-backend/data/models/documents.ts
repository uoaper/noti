import { DataTypes } from 'sequelize';
import { Json } from 'sequelize/types/lib/utils';

import  { sequelize }  from '../index'

const db = sequelize;

export interface IDocument {
  id?: number;
  preset: string;
  state: string;
  previousState: string;
  json: Json,
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
}
export const Document: any = db.define('document', {
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
},
  preset: {
    type: DataTypes.STRING,
    allowNull: false
  },
  state: {
    type: DataTypes.STRING,
    allowNull: false
  },
  previousState: {
    type: DataTypes.STRING,
    allowNull: false
  }

}, {
  // Other model options go here
  paranoid: true,
  timestamps: true
});

