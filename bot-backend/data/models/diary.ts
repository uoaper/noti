import { DataTypes } from 'sequelize';
import { Json } from 'sequelize/types/lib/utils';

import  { sequelize }  from '../index'

const db = sequelize;

export interface IDiary {
  id?: number;
  text: string;
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
}
export const Diary: any = db.define('diary', {
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
},
  text: {
    type: DataTypes.TEXT,
    allowNull: false
  }

}, {
  // Other model options go here
  paranoid: true,
  timestamps: true
});

