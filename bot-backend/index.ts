import * as TelegramBot from 'node-telegram-bot-api'


import * as config from 'config';
import * as pg from 'pg';

import { InitDb } from './data/index'

import * as models from './data/models';

import { Diary } from './data/models'
console.log('Models:', Object.keys(models).length);


const conString = config.get("db_connect");

const client = new pg.Client(conString);
const TOKEN = config.get("token");

console.log('our DB connection string --->', conString);
console.log ('our telegram TOKEN --->', TOKEN);
const bot = new TelegramBot(TOKEN, { polling: true });

// Pg client initialization 
client.connect();

// Sequelize initialization
InitDb();


//Тест пресетов
bot.onText(/start/, async (msg, [source, match]) => {

    console.log ('hohoh we are here')
    const { chat: { id } } = msg;
 

    const state = await client.query(`SELECT
    *
        from presets
    where preset like 'test'`)

    bot.sendMessage(id, 'Diary saved');
});


var importanceKeyboard = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{ text: '1', callback_data: '1' }],
            [{ text: '2', callback_data: '2' }],
            [{ text: '3', callback_data: '3' }]
        ]
    })
};

bot.onText(/\ididit/, (msg, [source, match]) => {
    const { chat: { id } } = msg;
    const text = msg.text.substr(6);

    bot.sendMessage(id, 'How important it was?'), importanceKeyboard;


    bot.on('message', msg => {

        const { chat: { id } } = msg;
        const importance = msg.text;


        bot.sendMessage(id, 'Project?');

        bot.on('message', msg => {

            const { chat: { id } } = msg;
            const project = msg.text;

            client.query('INSERT INTO test(text, importance, project) VALUES ($1,$2, $3)', [text, importance, project]);

            bot.sendMessage(id, 'IDidIt saved')

        });
    });
});


//Записываем Diary
bot.onText(/diary/, async (msg, [source, match]) => {

    state = 'diary_start';
    const { chat: { id } } = msg;
    
    bot.sendMessage(id, 'Write a diary record');
    
//    const textDiary = msg.text.substr(6);


   // const a = await Diary.create({
   //     text: textDiary
  //  })
   //     console.log('saving diary!!!', a)

});

//Bot ping
bot.onText(/ping/, (msg, [source, match]) => {

    bot.sendMessage(msg.chat.id, 'pong-hohoho');
});


let state = '';


let adversity;
let believ;
let conseq;
let dispute;
let energy; 
let diaryRecord; 
bot.onText(/nupsa/, function (msg, match) {

    state = "start"

    const { chat: { id }} = msg;
    bot.sendMessage(id, 'Your adversity honey?(Adversity)');

});

bot.onText(/\/clear/, function (msg, match) {

    state = "start"
    bot.sendMessage(msg.chat.id, 'Вернулись в начало - статус:' + state);

});

bot.on('message', async msg => {


    switch (state) {
        case "start":
            adversity = writeText(msg, "Your belief?(Belief)", "belief")
            break;

        case "belief":
            believ = writeText(msg,  "And consequences?(Consequences)", "conseq")
            break;
        
        case "conseq":
            conseq = writeText(msg, "Dispute it with you?(Dipsutation)", "dispute")
            break;

        case "dispute":
            dispute = writeText(msg,  "Let's fing some way out(Energization)", "energy")
            break;
         
        case "diary_start":
           await saveDiaryRecord(msg)
           
           break;

        case "energy":
           
            //console.log (adversity)
            try { 
               const result =  await client.query(`INSERT INTO main(adversity, belief, conseq, disput, energy,token_holder, facebook_id) 
            VALUES ('${adversity}','${believ}', '${conseq}', '${dispute}', '${energy})', 'telegrambot', 10215343077032609) RETURNING *;`)
            console.log(`result inserting ${JSON.stringify(result)}`)
            const recordNumber = result.rows[0].id-378
            energy = writeText(msg, `Nupsa saved, record ${recordNumber} in current treatment process, ${200-recordNumber} left`, "end" )
            } catch (e) {
                console.log('db error -->', e)
            }
            adversity = "";
            believ = "";
            conseq = "";
            dispute = "";
            energy = "";
            break;
    }

})

function writeText(msg, nextQuestionText, nextState) {
   
    bot.sendMessage(msg.chat.id, nextQuestionText);
    state = nextState;  

    return msg.text;
}

async function saveDiaryRecord(msg) {

  const textDiary = msg.text;

  state = "end"

   const a = await Diary.create({
   text: textDiary
   })
   
       bot.sendMessage(msg.chat.id, `Diary saved - record id >> ${a.id}` );
}
