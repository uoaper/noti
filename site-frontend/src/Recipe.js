import React from 'react';
import style from './recipe.module.css'
import moment from 'moment'


const Recipe = ({
     text,
     createdAt
    }) => {
    return(
        <div className={style.recipe}>
            <p>{text}</p>
            <p className={style.createdAt}>Добавлено: {moment(createdAt).format('DD.MM.YYYY HH:mm')}</p>
        </div>
    );
};

export default Recipe;

