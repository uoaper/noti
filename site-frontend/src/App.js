import logo from './logo.svg';
import { useEffect, useState } from "react";
import  Recipe  from './Recipe'
import './App.css';

import { w3cwebsocket as W3CWebSocket } from 'websocket'

const webSocketClient = new W3CWebSocket('ws://127.0.0.1:8000')

const App = () => {

  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState("");
  const [query, setQuery] = useState('chicken');

  useEffect(() => {
    console.log("Effect run");

    webSocketClient.onopen = () => {
      console.log('WebSocket Client Connected');
    };
    webSocketClient.onmessage = (message) => {
      console.log(message);
      };

    getData();

    }, [query]);

    const getData = async () => {

      const response = await fetch('https://www.nupsa.me/site-backend/diary/read')

      const data = await response.json();

      setRecipes(data);
    }

    const updateSearch = e => {
      setSearch(e.target.value);
      console.log(search)
    }

    const getSearch = e => {
      e.preventDefault();
      setQuery(search);
      setSearch("");
    }

  return (
    <div className="App">
      <h1 className="search-form">...</h1>
      <div className="recipes">
      {
        recipes.map(recipe => (
          <Recipe 
        text ={recipe.Text}
        createdAt = {recipe.CreatedAt}
        />
      ))}
      </div>
    </div>
  );
}

export default App;
