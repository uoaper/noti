// Copyright 2010 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build ignore

package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"

	"site-backend/models"

	_ "github.com/lib/pq"
)

type Page struct {
	Title string
	Body  []byte
}

func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) {

	// w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Access-Control-Allow-Headers", "Authorization")
	fmt.Println("We are in viewHandler")
	p, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/site-backend/edit/"+title, http.StatusFound)
		return
	}
	renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		p = &Page{Title: title}
	}
	renderTemplate(w, "edit", p)
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	p := &Page{Title: title, Body: []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/site-backend/view/"+title, http.StatusFound)
}

var templates = template.Must(template.ParseFiles("edit.html", "view.html"))

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

var validPath = regexp.MustCompile("^/site-backend/(edit|save|view|diary)/([a-zA-Z0-9]+)$")

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Access-Control-Allow-Origin", "*")

		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

		fmt.Println("request-->", r, r.URL.Path)

		fmt.Println("validPath -->", validPath)
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			fmt.Println("we are here probably")
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

func diaryRead(w http.ResponseWriter, r *http.Request, title string) {

	// туториал по БД https://www.calhoun.io/connecting-to-a-postgresql-database-with-gos-database-sql-package/
	// https://www.alexedwards.net/blog/organising-database-access
	// https://techinscribed.com/different-approaches-to-pass-database-connection-into-controllers-in-golang/

	fmt.Println("we are in the diary Read")

	// open DB connection
	bks, err := models.AllDiary()
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(500), 500)
		return
	}

	// var diaryEntries []models.DiaryEntry = []models.DiaryEntry;
	for _, bk := range bks {
		// fmt.Fprintf(w, "%s, %s", bk.Id, bk.Text)
		fmt.Println(w, bk.Id, bk.Text)
		// fn(w, r, )

	}
	json.NewEncoder(w).Encode(bks)
}

func main() {
	// init DB

	var err error

	// Initalize the sql.DB connection pool and assign it to the models.DB
	// global variable.
	models.DB, err = sql.Open("postgres",
		"user=postgres password=2017Variankalove dbname=nupsa sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	// htpp.

	http.HandleFunc("/site-backend/view/", makeHandler(viewHandler))
	http.HandleFunc("/site-backend/edit/", makeHandler(editHandler))
	http.HandleFunc("/site-backend/save/", makeHandler(saveHandler))

	http.HandleFunc("/site-backend/diary/read", makeHandler(diaryRead))

	fmt.Println("---- Go site backend started ----")

	log.Fatal(http.ListenAndServe(":8080", nil))
}
