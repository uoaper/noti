package models

import (
	"database/sql"
	"fmt"
	"time"
)

// Create an exported global variable to hold the database connection pool.
var DB *sql.DB

type DiaryEntry struct {
	Id        int
	Text      string
	CreatedAt time.Time
}

// AllDiary returns a slice of all Diary in the Diary table.
func AllDiary() ([]DiaryEntry, error) {
	// Note that we are calling Query() on the global variable.
	rows, err := DB.Query(
		"SELECT id, text, \"createdAt\" FROM diaries where text <> '' ORDER by id desc")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var bks []DiaryEntry

	fmt.Println(rows)

	for rows.Next() {
		var bk DiaryEntry

		fmt.Println("row", &bk.Id, &bk.Text, &bk.CreatedAt)

		err := rows.Scan(&bk.Id, &bk.Text, &bk.CreatedAt)

		if err != nil {
			return nil, err
		}

		bks = append(bks, bk)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return bks, nil
}
